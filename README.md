# Hug
A dns bind server made nicer with rust.

## Installation

### Dependencies
* A UNIX Dev environment with clang, make, etc. Or Docker.
* [Rust](https://www.rust-lang.org) or Docker

### Install
`make clean && make install`

### Docker
You can play around with it without installing Rust by using Docker.
```
docker-compose build
docker-compose up
```

## Usage
`/usr/local/sbin/hugd`

## Contributing
I am not accepting Pull Requests at this time.

## License
MIT License.

## Project status
This is just project to learn DNS and Rust.
