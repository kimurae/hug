//! OS native polling for incoming events and dispatches them to request handlers.

use std::{
    io,
    net::{SocketAddr,UdpSocket},
    sync::{
        Arc,
        mpsc::{Receiver, Sender, SendError, TryRecvError},
    }
};

use log::trace;

use crate::BubbleError;

mod worker;

pub(crate) use {
    worker::{Task, Worker},
};

use worker::{DequeuesTasks, Spawnable};

const MAX_PACKET_SIZE: usize = 512;

pub(crate) fn handle_socket<F: ForwardsMessages, R: ReceivesUdpBuffers>(messenger: F, listener: R) -> Result<(), BubbleError> {
    let mut buf: UdpBuffer = [0; MAX_PACKET_SIZE];

    trace!("[Dispatch] : Listening to socket.");
    let (_, source) = listener.recv_from(&mut buf)?;
    trace!("[{}] Received {:?}", source, buf);

    messenger.send(Message::Buffer((buf, source)))?;

    Ok(())
}

pub(crate) fn spawn<D: DequeuesTasks, T: Spawnable>(
    task: T,
    task_consumer: D,
) -> Result<Worker<D>, BubbleError> {
    let worker = Worker { task_consumer };
    task.spawn()?;

    Ok(worker)
}

pub(crate) struct Dispatcher<'dispatch_channel>(pub &'dispatch_channel Sender<Message>);

impl<'dispatch_channel> ForwardsMessages for Dispatcher<'dispatch_channel> {
    fn send(&self, t: Message) -> Result<(), SendError<Message>> {
        self.0.send(t)
    }
}

pub(crate) struct TaskListener(pub Receiver<Arc<Task>>);

impl DequeuesTasks for TaskListener {
    fn try_recv(&self) -> Result<Arc<Task>, TryRecvError> {
        self.0.try_recv()
    }
}
pub(crate) struct UdpListener<'udp_listener>(pub &'udp_listener UdpSocket);

impl<'udp_listener> ReceivesUdpBuffers for UdpListener<'udp_listener> {
    fn recv_from(&self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)> {
        self.0.recv_from(buf)
    }
}
 
pub(crate) trait ForwardsMessages {
    fn send(&self, t: Message) -> Result<(), SendError<Message>>;
}

pub(crate) trait ReceivesMessages {
    fn try_recv(&self) -> Result<Message, TryRecvError>;
}

pub(crate) trait ReceivesUdpBuffers {
    fn recv_from(&self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)>;
}

#[derive(Debug, PartialEq)]
pub(crate) enum Message {
    Buffer((UdpBuffer, SocketAddr)),
    Stop,
}


pub(crate) type UdpBuffer = [u8; MAX_PACKET_SIZE];

#[cfg(test)]
mod test {
    use super::*;
    use std::{
        io,
        sync::mpsc::{channel, sync_channel, Receiver, Sender},
        net::{Ipv4Addr, SocketAddr, SocketAddrV4}
    };

    #[test]
    fn test_handle_socket_on_success() -> Result<(), BubbleError> {

        let (sender, receiver) : (Sender<Message>, Receiver<Message>) = channel();

        let socket = UdpSocket::bind("0.0.0.0:9999")?;

        let buf : [u8; 512] = [42u8; 512];
        
        let addr = SocketAddrV4::new(Ipv4Addr::new(127,0,0,1), 9999);
        let source = SocketAddr::V4(addr);

        socket.send_to(&buf, addr)?;

        handle_socket(Dispatcher(&sender), UdpListener(&socket))?;

        let message : Message = receiver.recv()?;

        assert_eq!(message, Message::Buffer((buf, source)));

        Ok(())
    }

    #[test]
    fn test_handle_socket_on_failure() {

        struct MockUdpListener;

        impl ReceivesUdpBuffers for MockUdpListener {
            fn recv_from(&self, _buf: &mut [u8]) -> io::Result<(usize, SocketAddr)> {
                let error = io::Error::from(io::ErrorKind::UnexpectedEof);

                Err(error)
            }
        }

        let (sender, _receiver) : (Sender<Message>, Receiver<Message>) = channel();

        assert!(handle_socket(Dispatcher(&sender), MockUdpListener{}).is_err());
    }

    #[test]
    fn test_handle_socket_on_failed_sender() {

        struct MockDispatcher;

        impl ForwardsMessages for MockDispatcher {
            fn send(&self, t: Message) -> Result<(), SendError<Message>> {
                let error : SendError<Message> = SendError(t);
                Err(error)
            }
        }

        let socket = UdpSocket::bind("0.0.0.0:9998").unwrap();

        let buf : [u8; 512] = [42u8; 512];
        
        let addr = SocketAddrV4::new(Ipv4Addr::new(127,0,0,1), 9998);

        socket.send_to(&buf, addr).unwrap();

        assert!(handle_socket(MockDispatcher{}, UdpListener(&socket)).is_err());
    }
    
    #[test]
    fn test_spawn() {
        struct MockTask;

        impl Spawnable for MockTask {
            fn spawn(self) -> Result<(), BubbleError> {
                Ok(())
            }
        }

        let (_, receiver) = sync_channel(1);

        let result = spawn(MockTask{}, TaskListener(receiver));

        assert!(result.is_ok());
    }
    #[test]
    fn test_spawn_gone_wrong() {

        struct MockTask;

        impl Spawnable for MockTask {
            fn spawn(self) -> Result<(), BubbleError> {
                let error : SendError<MockTask> = SendError(self);
                Err(Box::new(error))
            }
        }

        let (_, receiver) = sync_channel(1);

        assert!(spawn(MockTask{}, TaskListener(receiver)).is_err());
    }
}
