use std::{
    future::Future,
    pin::Pin,
    sync::{
        mpsc::{TryRecvError, SyncSender},
        Arc, Mutex, MutexGuard,
    },
    task::{Context, Poll, Wake, Waker},
};

use crate::BubbleError;

use log::warn;

type PerformMethod = Pin<Box<dyn Future<Output = Result<(), BubbleError>> + Send + 'static>>;

pub struct Worker<D: DequeuesTasks> {
    pub task_consumer: D,
}

impl<D: DequeuesTasks> Worker<D> {
    pub fn run(&self) -> Result<(), BubbleError> {
        match self.task_consumer.try_recv() {

            Err(TryRecvError::Empty) => Ok(()),
            Err(e) => Err(Box::new(e)),
            Ok(task) => {
                let waker: Waker = task.clone().into();
                let context = &mut Context::from_waker(&waker);

                task.perform(context)?;

                Ok(())
            }
        }
    }
}

pub struct Task {
    pub perform: Mutex<Option<PerformMethod>>,
    pub producer: SyncSender<Arc<Self>>, 
}

impl Performable for Task {
    fn perform(&self, context: &mut Context<'_>) -> Result<(), BubbleError> {
        let mut possible_future: MutexGuard<Option<PerformMethod>> =
                    self.perform.lock().map_err(TaskError::from_poison_error)?;

        let mut future: PerformMethod =
            possible_future.take().ok_or(Box::new(TaskError {
                message: "No perform method.".to_string(),
            }))?;

        match future.as_mut().poll(context) {
            Poll::Ready(r) => r,
            Poll::Pending => {
                *possible_future = Some(future);
                Ok(())
            }
        }
    }
}

impl Spawnable for Task {
    fn spawn(self) -> Result<(), BubbleError> {
        let producer = &self.producer.clone();

        producer.send(Arc::new(self))?;

        Ok(())
    }
}

impl Wake for Task {
    fn wake(self: Arc<Self>) {
        let cloned: Arc<Self> = self.clone();

        if let Err(e) = self.producer.send(cloned) {
            warn!("[Task] Unable to awaken. {}", e);
        }
    }
}

pub trait DequeuesTasks {
    fn try_recv(&self) -> Result<Arc<Task>, TryRecvError>;
}

pub trait Performable {
    fn perform(&self, context: &mut Context<'_>) -> Result<(), BubbleError>;
}

pub trait Spawnable {
    fn spawn(self) -> Result<(), BubbleError>;
}

#[derive(Debug)]
struct TaskError {
    message: String,
}

impl TaskError {
    fn from_poison_error(e: impl ToString) -> TaskError {
        let message: String = e.to_string();

        TaskError { message }
    }
}
impl std::fmt::Display for TaskError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Task Error: {}", self.message)
    }
}

impl std::error::Error for TaskError {}

#[cfg(test)]
mod test {

    use super::*;
    use crate::dispatch::TaskListener;

    use std::{
        future::Future,
        sync::mpsc::sync_channel,
    };

    struct OkFuture;

    impl Future for OkFuture {
        type Output = Result<(), BubbleError>;

        fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
            Poll::Ready(Ok(()))
        }
    }

    struct BadFuture;

    impl Future for BadFuture {
        type Output = Result<(), BubbleError>;

        fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
            Poll::Ready(Err(Box::new(TaskError { message: "Meow".to_string()})))
        }

    }


    struct PendingFuture;

    impl Future for PendingFuture {
        type Output = Result<(), BubbleError>;

        fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
            Poll::Pending
        }

    }

        #[test]
    fn test_worker_run() {
        let (producer, task_consumer) = sync_channel::<Arc<Task>>(1);

        let task = Task{ producer, perform: Mutex::new(Some(Box::pin(OkFuture{}))) };

        let worker = Worker{ task_consumer: TaskListener(task_consumer) };

        task.spawn().unwrap();

        assert!(worker.run().is_ok());
    }

    #[test]
    fn test_worker_run_pending_future() {
        let (producer, task_consumer) = sync_channel::<Arc<Task>>(1);

        let task = Task{ producer, perform: Mutex::new(Some(Box::pin(PendingFuture{}))) };

        let worker = Worker{ task_consumer: TaskListener(task_consumer) };

        task.spawn().unwrap();

        assert!(worker.run().is_ok());
    }

    #[test]
    fn test_worker_run_bad_future() {
        let (producer, task_consumer) = sync_channel::<Arc<Task>>(1);

        let task = Task{ producer, perform: Mutex::new(Some(Box::pin(BadFuture{}))) };

        let worker = Worker{ task_consumer: TaskListener(task_consumer) };

        task.spawn().unwrap();

        assert!(worker.run().is_err());
    }

    #[test]
    fn test_worker_run_disconnected() {
        struct DisconnectedChannel;

        impl DequeuesTasks for DisconnectedChannel {
            fn try_recv(&self) -> Result<Arc<Task>, TryRecvError> {
                Err(TryRecvError::Disconnected)
            }
        }

        let worker = Worker { task_consumer: DisconnectedChannel{} };

        assert!(worker.run().is_err());
    }

    #[test]
    fn test_worker_run_empty() {
        struct EmptyChannel;

        impl DequeuesTasks for EmptyChannel {
            fn try_recv(&self) -> Result<Arc<Task>, TryRecvError> {
                Err(TryRecvError::Empty)
            }
        }

        let worker = Worker { task_consumer: EmptyChannel{} };

        assert!(worker.run().is_ok());
    }

    #[test]
    fn test_task_error() {
        let task_error = TaskError{ message: "Mmm".to_string() };

        assert_eq!( task_error.to_string(), "Task Error: Mmm");
        assert_eq!(format!("[Worker] {:?}", task_error), "[Worker] TaskError { message: \"Mmm\" }");

        assert_eq!( TaskError::from_poison_error("Merg!").to_string(), "Task Error: Merg!" );

    }
}
