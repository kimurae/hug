//! Stores current client request state in an easy to search map.

use std::{
   collections::BTreeMap,
   net::{ Ipv4Addr, SocketAddr },
};

use crate::{
    BubbleError,
    dns::url::Url,
};

#[derive(Clone)]
pub(crate) struct RequestMap(BTreeMap<u16, Vec<Query>>);

impl RequestMap {
    pub(crate) fn new() -> Self {
        let btm: BTreeMap<u16, Vec<Query>> = BTreeMap::new();

        Self(btm)
    }

    pub(super) fn insert(&mut self, id: u16, name_server: Ipv4Addr, source: SocketAddr, url: Url) -> Query {
        let query: Query = Query {
            id,
            name_server,
            source,
            url,
        };

        let q = self.0.entry(id).or_insert_with(|| Vec::with_capacity(2));

        q.push(query.clone());

        return query;
    }

    /// Checks if there is only one request in the stack.
    /// 
    /// The RequestMap stores a stack for each request by the unique identifier
    /// the client sent to this server (Per DNS Spec).
    /// It is important to know if there is only one or zero elements left in the stack
    /// when an answer in the form of an ip address is returned, as that signals the server
    /// that we have no further work, and we can return that response back to the client.
    pub(super) fn is_singular(&self, id: &u16) -> bool {
        self.0
            .get(id)
            .and_then(|cat| Some(cat.len() <= 1))
            .unwrap_or(false)
    }
    pub(super) fn source_for(&self, id: &u16) -> Result<SocketAddr, BubbleError> {
        let query: &Query = self.current_for(id)?;

        Ok(query.source)
    }

    pub(super) fn pop(&mut self, id: &u16) -> Result<Query, BubbleError> {
        let query: Query = self
            .0
            .get_mut(id)
            .and_then(|i| i.pop())
            .ok_or(Self::no_data(*id))?;

        Ok(query)
    }

    pub(super) fn replace_ns(&mut self, id: &u16, name_server: Ipv4Addr) -> Result<Query, BubbleError> {
        let query: Query = self
            .0
            .get_mut(id)
            .and_then(|stack: &mut Vec<Query>| -> Option<Query> {
                stack.pop().and_then(
                    |Query {
                         id,
                         source,
                         url,
                         name_server: _,
                     }|
                     -> Option<Query> {
                        let new_query: Query = Query {
                            id,
                            name_server,
                            source,
                            url,
                        };

                        stack.push(new_query.clone());

                        Some(new_query)
                    },
                )
            })
            .ok_or(Self::no_data(*id))?;

        Ok(query)
    }

    // Fetch the current request for the unique identifier.
    fn current_for(&self, id: &u16) -> Result<&Query, BubbleError> {
        let query: &Query = self
            .0
            .get(id)
            .and_then(|i| i.last())
            .ok_or(Self::no_data(*id))?;

        Ok(query)
    }

    // returns a Bubble when there is no data for a given id.
    fn no_data(id: u16) -> BubbleError {
        Box::new(DataNotFoundError { id })
    }
}

#[derive(Debug)]
struct DataNotFoundError {
    id: u16,
}

impl std::error::Error for DataNotFoundError {}

impl std::fmt::Display for DataNotFoundError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "No data found for {}", self.id)
    }
}

#[derive(Clone, Debug)]
pub(super) struct Query {
    pub(super) id: u16,
    pub(super) name_server: Ipv4Addr,
    pub(super) source: SocketAddr,
    pub(super) url: Url,
}


#[cfg(test)]
mod test {

    use super::*;

    use std::net::SocketAddrV4;

    #[test]
    fn test_request_map() {
        let mut requests = RequestMap::new();

        let data_not_fnd = "No data found for 42".to_string();
        let addr = SocketAddrV4::new(Ipv4Addr::new(127,0,0,1), 9999);
        let source = SocketAddr::V4(addr);

        assert_eq!(requests.is_singular(&42), false);
        assert_eq!(requests.source_for(&42).unwrap_err().to_string(), data_not_fnd);
        assert_eq!(requests.pop(&42).unwrap_err().to_string(), data_not_fnd);
        assert_eq!(requests.replace_ns(&42, Ipv4Addr::new(0,0,0,0)).unwrap_err().to_string(), data_not_fnd);

        requests.insert(42, Ipv4Addr::new(8,8,8,8), source, Url::from("www.example.com".to_string()));

        assert_eq!(requests.is_singular(&42), true);
        assert_eq!(requests.source_for(&42).unwrap(), source);

        let request : Query = requests.replace_ns(&42, Ipv4Addr::new(0,0,0,0)).unwrap();
        assert_eq!(request.name_server, Ipv4Addr::new(0,0,0,0));

        let request : Query = requests.pop(&42).unwrap();

        assert_eq!(request.id, 42);
        assert_eq!(request.name_server, Ipv4Addr::new(0,0,0,0));
        assert_eq!(request.source, source);
        // assert_eq!(request.url.to_string(), "www.example.com".to_string());
    }

    #[test]
    fn test_data_not_found_error() {
        let error = DataNotFoundError{ id: 42 };

        assert_eq!(format!("{}", error), "No data found for 42");
    }
}
