//! A future for the work involved with handling a client request.

use std::{
    boxed::Box,
    future::Future,
    pin::Pin,
    sync::{
        Arc, Mutex, MutexGuard, TryLockError, TryLockResult,
    },
    task::{Context, Poll},
};

use log::error;

use crate::BubbleError;

use super::{
    handler::{Handler, ReceivesMessages, UdpResponder},
    request_map::RequestMap
};

pub(crate) struct RequestFuture<R: ReceivesMessages,U: UdpResponder> {
    shared_state: Arc<Mutex<Handler<R,U>>>,
}

impl<R: ReceivesMessages, U: UdpResponder> RequestFuture<R,U> {
    pub(crate) fn new(
        consumer: R,
        requests: Arc<Mutex<RequestMap>>,
        socket: U,
    ) -> Self {
        let shared_state = Arc::new(Mutex::new(Handler {
            consumer,
            requests,
            socket,
            waker: None,
        }));

        RequestFuture { shared_state }
    }

    pub(crate) fn perform(self) -> Pin<Box<Self>> {
        Box::pin(self)
    }
}

impl<R: ReceivesMessages, U: UdpResponder> Future for RequestFuture<R,U> {
    type Output = Result<(), BubbleError>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mutex: TryLockResult<MutexGuard<Handler<R,U>>> = self.shared_state.try_lock();

        match mutex.and_then(|mut shared_state| match shared_state.handle() {
            Ok(true) => {
                shared_state.set_waker(cx.waker());
                Ok(Poll::Pending)
            }
            Ok(false) => Ok(Poll::Ready(Ok(()))),
            Err(e) => Ok(Poll::Ready(Err(e))),
        }) {
            Ok(poll) => poll,
            Err(TryLockError::WouldBlock) => Poll::Pending,
            Err(TryLockError::Poisoned(e)) => {
                error!("[Worker][Error] {}", e);
                Poll::Ready(Err(Box::new(RequestError::from_poison_error(e))))
            }
        }
    }
}

#[derive(Debug)]
struct RequestError {
    message: String,
}

impl RequestError {
    fn from_poison_error(e: impl ToString) -> RequestError {
        let message: String = e.to_string();
        RequestError { message }
    }
}
impl std::fmt::Display for RequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for RequestError {}

#[cfg(test)]
mod test {

    use std::{
        io,
        net::{Ipv4Addr, SocketAddr, SocketAddrV4, ToSocketAddrs},
        sync::mpsc::TryRecvError,
        task::{Wake,Waker}
    };

    use super::*;

    use crate::dispatch::{Message, UdpBuffer};

    #[test]
    fn test_request_future() {
        struct MockReceiver;
        struct MockUdpResponder;
        struct MockWaker;

        impl ReceivesMessages for MockReceiver {
            fn try_recv(&self) -> Result<Message, TryRecvError> {
                let buf : UdpBuffer = [0u8; 512];
                let source = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0,0,0,0), 99));

                Ok(Message::Buffer((buf, source)))
            }
        }

        impl UdpResponder for MockUdpResponder {
            fn send_to<A: ToSocketAddrs>(&self, _buf: &[u8], _addr: A) -> io::Result<usize> {
                Ok(1)
            }
        }

        impl Wake for MockWaker {
            fn wake(self: Arc<Self>) {}
        }

        let requests = Arc::new(Mutex::new(RequestMap::new()));

        let mut future = RequestFuture::new(MockReceiver{}, requests, MockUdpResponder{}).perform();

        let waker : Waker = Arc::new(MockWaker{}).into();

        let mut context : Context = Context::from_waker(&waker);

        assert_eq!(future.as_mut().poll(&mut context).is_pending(), false);

    }

    #[test]
    fn test_request_future_with_stop() {
        struct MockReceiver;
        struct MockUdpResponder;
        struct MockWaker;

        impl ReceivesMessages for MockReceiver {
            fn try_recv(&self) -> Result<Message, TryRecvError> {
                Ok(Message::Stop)
            }
        }

        impl UdpResponder for MockUdpResponder {
            fn send_to<A: ToSocketAddrs>(&self, _buf: &[u8], _addr: A) -> io::Result<usize> {
                Ok(1)
            }
        }

        impl Wake for MockWaker {
            fn wake(self: Arc<Self>) {}
        }

        let requests = Arc::new(Mutex::new(RequestMap::new()));

        let mut future = RequestFuture::new(MockReceiver{}, requests, MockUdpResponder{}).perform();

        let waker : Waker = Arc::new(MockWaker{}).into();

        let mut context : Context = Context::from_waker(&waker);

        assert!(future.as_mut().poll(&mut context).is_ready());

    }

    #[test]
    fn test_request_future_with_disconnect() {
        struct MockReceiver;
        struct MockUdpResponder;
        struct MockWaker;

        impl ReceivesMessages for MockReceiver {
            fn try_recv(&self) -> Result<Message, TryRecvError> {
                Err(TryRecvError::Disconnected)
            }
        }

        impl UdpResponder for MockUdpResponder {
            fn send_to<A: ToSocketAddrs>(&self, _buf: &[u8], _addr: A) -> io::Result<usize> {
                Ok(1)
            }
        }

        impl Wake for MockWaker {
            fn wake(self: Arc<Self>) {}
        }

        let requests = Arc::new(Mutex::new(RequestMap::new()));

        let mut future = RequestFuture::new(MockReceiver{}, requests, MockUdpResponder{}).perform();

        let waker : Waker = Arc::new(MockWaker{}).into();

        let mut context : Context = Context::from_waker(&waker);

        assert!(future.as_mut().poll(&mut context).is_ready());

    }

    #[test]
    fn test_request_future_with_empty() {
        struct MockReceiver;
        struct MockUdpResponder;
        struct MockWaker;

        impl ReceivesMessages for MockReceiver {
            fn try_recv(&self) -> Result<Message, TryRecvError> {
                Err(TryRecvError::Empty)
            }
        }

        impl UdpResponder for MockUdpResponder {
            fn send_to<A: ToSocketAddrs>(&self, _buf: &[u8], _addr: A) -> io::Result<usize> {
                Ok(1)
            }
        }

        impl Wake for MockWaker {
            fn wake(self: Arc<Self>) {}
        }

        let requests = Arc::new(Mutex::new(RequestMap::new()));

        let mut future = RequestFuture::new(MockReceiver{}, requests, MockUdpResponder{}).perform();

        let waker : Waker = Arc::new(MockWaker{}).into();

        let mut context : Context = Context::from_waker(&waker);

        assert!(future.as_mut().poll(&mut context).is_pending());
    }

    #[test]
    fn test_request_future_with_socket_error() {
        struct MockReceiver;
        struct MockUdpResponder;
        struct MockWaker;

        impl ReceivesMessages for MockReceiver {
            fn try_recv(&self) -> Result<Message, TryRecvError> {
                let buf : UdpBuffer = [0u8; 512];
                let source = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0,0,0,0), 99));

                Ok(Message::Buffer((buf, source)))
            }
        }

        impl UdpResponder for MockUdpResponder {
            fn send_to<A: ToSocketAddrs>(&self, _buf: &[u8], _addr: A) -> io::Result<usize> {
                Err(io::Error::new(io::ErrorKind::Other, "test"))
            }
        }

        impl Wake for MockWaker {
            fn wake(self: Arc<Self>) {}
        }

        let requests = Arc::new(Mutex::new(RequestMap::new()));

        let mut future = RequestFuture::new(MockReceiver{}, requests, MockUdpResponder{}).perform();

        let waker : Waker = Arc::new(MockWaker{}).into();

        let mut context : Context = Context::from_waker(&waker);

        assert!(future.as_mut().poll(&mut context).is_ready());
    }

    #[test]
    fn test_request_error() {
        let error = RequestError::from_poison_error("Foo");

        assert_eq!(format!("{}", error), "Foo");
    }
}
