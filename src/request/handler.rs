//! Handles a client request.

use std::{
    boxed::Box,
    io,
    net::{IpAddr, Ipv4Addr, SocketAddr, ToSocketAddrs},
    sync::{
        mpsc::TryRecvError,
        Arc, Mutex,
    },
    task::Waker,
};

use crate::{
    dispatch::{Message, UdpBuffer},
    dns::{ParseError, Payload},
    BubbleError,
};

use super::request_map::{Query, RequestMap};

const ROOT_SERVER: Ipv4Addr = Ipv4Addr::new(198, 41, 0, 4);

pub(super) struct Handler<R: ReceivesMessages, U: UdpResponder> {
    pub consumer: R,
    pub requests: Arc<Mutex<RequestMap>>,
    pub socket: U,
    pub waker: Option<Waker>,
}

impl<R: ReceivesMessages, U: UdpResponder> Handler<R, U> {
    pub fn handle(&mut self) -> Result<bool, BubbleError> {
        match self.consumer.try_recv() {
            Ok(Message::Buffer((buf, source))) => {
                let parcel: Parcel = self.parse_buffer(&buf, source)?;
                self.send(parcel.manifest())?;
                Ok(true)
            }
            Err(TryRecvError::Empty) => Ok(true),
            Ok(Message::Stop) => Ok(false),
            Err(e) => Err(Box::new(e)),
        }
    }

    pub fn set_waker(&mut self, waker: &Waker) {
        self.waker = Some(waker.clone());
    }

    fn parse_buffer(&mut self, buf: &UdpBuffer, source: SocketAddr) -> Result<Parcel, BubbleError> {
        let response = crate::dns::parse(buf)?;

        let id: u16 = response.id;

        let mut requests = self
            .requests
            .lock()
            .map_err(RequestError::from_poison_error)?;

        let query = match response.payload {
            Payload::Answer(_) if requests.is_singular(&id) => {
                (*requests).pop(&id)?;

                return Ok(Parcel::Response(Manifest {
                    addr: source.ip(),
                    buf: Box::new(buf.clone()),
                }));
            }
            Payload::Answer(name_server) => {
                (*requests).pop(&id)?;

                (*requests).replace_ns(&id, name_server)?
            }
            Payload::Domain(url) => {
                let source: SocketAddr = (*requests).source_for(&id)?;

                (*requests).insert(id, ROOT_SERVER, source, url)
            }
            Payload::Ip(name_server) => (*requests).replace_ns(&id, name_server)?,
            Payload::Question(_) if requests.is_singular(&id) => {
                return Err(Box::new(ParseError {
                    message: "Received Question instead of Response".to_string(),
                }));
            }
            Payload::Question(q) => (*requests).insert(id, ROOT_SERVER, source, q[0].name.clone()),
        };

        Ok(Parcel::Request(Manifest::from(query)))
    }

    fn send(&self, manifest: &Manifest) -> Result<(), BubbleError> {
        let addr = SocketAddr::new(manifest.addr, 53);

        self.socket.send_to(manifest.buf.as_ref(), addr)?;
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub(super) struct Manifest {
    addr: IpAddr,
    buf: Box<[u8]>,
}

impl From<Query> for Manifest {
    fn from(query: Query) -> Self {
        let addr: IpAddr = IpAddr::V4(query.name_server);
        let buf: Box<[u8]> = crate::dns::build_query(query.id, &query.url).into_boxed_slice();

        Manifest { addr, buf }
    }
}

pub(crate) trait ReceivesMessages {
    fn try_recv(&self) -> Result<Message, TryRecvError>;
}

pub(crate) trait UdpResponder {
    fn send_to<A: ToSocketAddrs>(&self, buf: &[u8], addr: A) -> io::Result<usize>;
}

enum Parcel {
    Response(Manifest),
    Request(Manifest),
}

impl Parcel {
    fn manifest(&self) -> &Manifest {
        match self {
            Self::Response(m) | Self::Request(m) => m,
        }
    }
}

#[derive(Debug)]
struct RequestError {
    message: String,
}

impl RequestError {
    fn from_poison_error(e: impl ToString) -> RequestError {
        let message: String = e.to_string();
        RequestError { message }
    }
}
impl std::fmt::Display for RequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for RequestError {}
