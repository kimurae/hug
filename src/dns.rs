pub mod buffer;
mod header;
mod question;
mod record;
mod response;
pub mod url;

pub(crate) use response::{parse, Payload};

pub fn build_query(id: u16, domain_name: &url::Url) -> Vec<u8> {
    let header = header::for_query(id);

    let question = question::Question::from(domain_name.clone());

    let be_query = [
        header.serialize().as_slice(),
        question.serialize().as_slice(),
    ]
    .concat()
    .to_vec();

    return be_query;
}

#[derive(Debug)]
pub struct ParseError {
    pub message: String,
}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for ParseError {}

impl From<Payload> for ParseError {
    fn from(p: Payload) -> Self {
        let message = format!("{:?}", p);

        Self { message }
    }
}

#[test]
fn test_build_query() {
    let query: Vec<u8> = build_query(0x8298, &url::Url::from("www.example.com".to_string()));

    let hex: String = query
        .iter()
        .map(|b| format!("{:02x}", b).to_string())
        .collect::<Vec<String>>()
        .join(" ");

    assert_eq!("82 98 00 00 00 01 00 00 00 00 00 00 03 77 77 77 07 65 78 61 6d 70 6c 65 03 63 6f 6d 00 00 01 00 01", hex);
}
