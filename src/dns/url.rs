use std::fmt;
use std::io::{Cursor, Read};

use crate::BubbleError;

const COMPRESSED_MASK: u8 = 0b11000000;

#[derive(Clone, Debug)]
pub struct Url(pub Box<[u8]>);

impl fmt::Display for Url {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl From<String> for Url {
    fn from(item: String) -> Self {
        let len: usize = item.len() + 1;

        let mut chunks: Vec<u8> = Vec::with_capacity(len);

        for chunk in item.split(".") {
            chunks.push(chunk.len() as u8);

            chunks.extend_from_slice(chunk.as_bytes());
        }

        chunks.push(b'\x00' as u8);

        Self(chunks.into_boxed_slice())
    }
}

pub fn decode_url<const N: usize>(buf: &mut Cursor<[u8; N]>) -> Result<String, BubbleError> {
    let mut parts: Vec<String> = Vec::new();

    let mut byte: [u8; 1] = [0u8; 1];

    loop {
        buf.read(&mut byte)?;

        let [pos] = byte;

        if pos == b'\x00' {
            break;
        }

        if is_compressed(&pos) {
            let mut ptr_byte: [u8; 1] = [0u8; 1];

            buf.read_exact(&mut ptr_byte)?;

            let [ptr_pos] = ptr_byte;

            let pointer: u64 = ((pos & 0b00111111) + ptr_pos) as u64;

            let part: String = decode_compressed_url(buf, pointer)?;
            parts.push(part);
            break;
        } else {
            let mut bytes: Vec<u8> = Vec::new();
            for _ in 0..pos {
                buf.read_exact(&mut byte)?;

                let [item] = byte;

                bytes.push(item);
            }
            let part = String::from_utf8(bytes)?;
            parts.push(part);
        }
    }

    return Ok(parts.join("."));
}

fn decode_compressed_url<const N: usize>(
    buf: &mut Cursor<[u8; N]>,
    pointer: u64,
) -> Result<String, BubbleError> {
    let current_pos: u64 = buf.position();

    buf.set_position(pointer);

    let url: String = decode_url(buf)?;

    buf.set_position(current_pos);

    return Ok(url);
}

fn is_compressed(byte: &u8) -> bool {
    byte & COMPRESSED_MASK == COMPRESSED_MASK
}

#[test]
fn test_decode_url() {
    let url: [u8; 11] = [3, 101, 101, 101, 5, 101, 101, 101, 101, 101, 0];

    let mut buf = Cursor::new(url);

    assert_eq!("eee.eeeee".to_string(), decode_url(&mut buf).unwrap());
}
