use super::buffer::Reader;
use super::url::{decode_url, Url};
use std::convert::TryFrom;
use std::io::{Cursor, Read};

use crate::BubbleError;

#[derive(Debug)]
pub struct Question {
    pub(crate) name: Url,
    type_: u16,
    class: u16,
}

impl Question {
    pub fn serialize(&self) -> Vec<u8> {
        let len: usize = 4 + self.name.0.len();
        let mut bytes: Vec<u8> = Vec::with_capacity(len);

        bytes.extend_from_slice(self.name.0.as_ref());
        bytes.extend(self.type_.to_be_bytes());
        bytes.extend(self.class.to_be_bytes());

        return bytes;
    }
}

impl From<Url> for Question {
    fn from(item: Url) -> Question {
        Self {
            name: item,
            type_: 1,
            class: 1,
        }
    }
}

impl From<String> for Question {
    fn from(item: String) -> Question {
        let name: Url = Url::from(item);

        Self {
            name,
            type_: 1,
            class: 1,
        }
    }
}

impl<const N: usize> TryFrom<&mut Reader<N>> for Question {
    type Error = BubbleError;

    fn try_from(buf: &mut Reader<N>) -> Result<Self, Self::Error> {
        let decoded_url: String = decode_url(buf)?;
        let type_: u16 = extract16(buf)?;
        let class: u16 = extract16(buf)?;
        let name: Url = Url::from(decoded_url);

        Ok(Self { name, type_, class })
    }
}

fn extract16<const N: usize>(buf: &mut Cursor<[u8; N]>) -> Result<u16, BubbleError> {
    let mut bytes: [u8; 2] = [0u8; 2];

    buf.read_exact(&mut bytes)?;

    Ok(u16::from_be_bytes(bytes))
}
