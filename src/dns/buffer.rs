use std::io::Cursor;

pub(super) type Reader<const N: usize> = Cursor<[u8; N]>;
