use log::trace;
use std::io::{Cursor, Read};
use std::net::Ipv4Addr;

const TYPE_A: u16 = 1;
const TYPE_NS: u16 = 2;

use crate::{
    dns::{
        header::Header,
        question::Question,
        record::{decode_records, Record},
        url::Url,
        ParseError,
    },
    BubbleError,
};

#[derive(Debug)]
pub(crate) struct Response {
    pub id: u16,
    pub payload: Payload,
}

#[derive(Debug)]
pub(crate) enum Payload {
    Answer(Ipv4Addr),
    Domain(Url),
    Ip(Ipv4Addr),
    Question(Vec<Question>),
}

pub(crate) fn parse<const N: usize>(packet: &[u8; N]) -> Result<Response, BubbleError> {
    let bytes: [u8; N] = *packet;

    let mut buf = Cursor::new(bytes);

    let mut head: [u8; 12] = [0u8; 12];

    buf.read_exact(&mut head)?;

    let header: Header<[u16; 6]> = Header::from(&head);

    if header.has_errors() {
        let message = format!("Response has errors, {:?}", header);
        return Err(Box::new(ParseError { message }));
    }

    let mut questions: Vec<Question> = Vec::new();

    for _ in 0..header.question_count() {
        let question: Question = Question::try_from(&mut buf)?;
        questions.push(question);
    }

    let answers: Vec<Record> = decode_records(&mut buf, header.answer_count())?;
    let authorities: Vec<Record> = decode_records(&mut buf, header.authority_count())?;
    let additionals: Vec<Record> = decode_records(&mut buf, header.additionals_count())?;

    let id: u16 = header.id();

    let payload: Payload = if header.answer_count() == 0 {
        if let Some(payload) = extract_next_dest(&additionals, TYPE_A) {
            payload
        } else if let Some(payload) = extract_next_dest(&authorities, TYPE_NS) {
            payload
        } else {
            let error: ParseError = ParseError {
                message: "Invalid buffer".to_string(),
            };
            return Err(Box::new(error));
        }
    } else if let Some(Payload::Ip(ip)) = extract_next_dest(&answers, TYPE_A) {
        Payload::Answer(ip)
    } else if header.is_question() {
        trace!("Question received {:?}{:?}", header, additionals);
        Payload::Question(questions)
    } else {
        let message: String = format!(
            "{:?}{:?}{:?}{:?}{:?}",
            header, questions, answers, authorities, additionals
        );

        let error: ParseError = ParseError { message };

        return Err(Box::new(error));
    };

    return Ok(Response { id, payload });
}

fn extract_next_dest(authorities: &Vec<Record>, type_: u16) -> Option<Payload> {
    if let Some(authority) = authorities.iter().filter(|&x| x.type_ == type_).nth(0) {
        match type_ {
            TYPE_A => {
                if let Some(ip) = authority.ip() {
                    return Some(Payload::Ip(ip));
                }
            }
            TYPE_NS => {
                if let Some(url) = authority.url() {
                    return Some(Payload::Domain(url));
                }
            }
            _ => return None,
        }
    }
    return None;
}
