use std::io::Read;
use std::net::Ipv4Addr;

use super::buffer::Reader;
use super::url::{decode_url, Url};
use crate::BubbleError;

const TYPE_A: u16 = 1;
const TYPE_NS: u16 = 2;

#[allow(dead_code)]
#[derive(Debug)]
pub struct Record {
    class: u16,
    data: Data,
    name: String,
    ttl: u32,
    pub type_: u16,
}

impl Record {
    pub fn ip(&self) -> Option<Ipv4Addr> {
        if let Data::Ip(ip) = self.data {
            Some(ip as Ipv4Addr)
        } else {
            None
        }
    }
    pub(super) fn url(&self) -> Option<Url> {
        if let Data::Url(url) = &self.data {
            Some(url.clone())
        } else {
            None
        }
    }
}

pub fn decode_records<const N: usize>(
    buf: &mut Reader<N>,
    len: u16,
) -> Result<Vec<Record>, BubbleError> {
    let mut records: Vec<Record> = Vec::new();

    for _ in 0..len {
        let record: Record = decode_record(buf)?;
        records.push(record);
    }

    Ok(records)
}

fn decode_record<const N: usize>(buf: &mut Reader<N>) -> Result<Record, BubbleError> {
    let name: String = decode_url(buf)?;
    let type_: u16 = extract16(buf)?;
    let class: u16 = extract16(buf)?;
    let ttl: u32 = extract32(buf)?;
    let data_len: usize = extract16(buf)? as usize;

    let data = match type_ {
        TYPE_A => {
            let mut ip: [u8; 4] = [0u8; 4];

            buf.read_exact(&mut ip)?;

            Data::Ip(Ipv4Addr::from(ip))
        }
        TYPE_NS => {
            let bytes: Box<[u8]> = extract_len(buf, data_len)?;
            let url: Url = Url(bytes);
            Data::Url(url)
        }
        _ => {
            let other: Box<[u8]> = extract_len(buf, data_len)?;

            Data::Other(other)
        }
    };

    let record: Record = Record {
        class,
        data,
        name,
        ttl,
        type_,
    };

    return Ok(record);
}

#[derive(Debug)]
enum Data {
    Ip(Ipv4Addr),
    Url(Url),
    Other(Box<[u8]>),
}

fn extract16<const N: usize>(buf: &mut Reader<N>) -> Result<u16, BubbleError> {
    let mut bytes: [u8; 2] = [0u8; 2];

    buf.read_exact(&mut bytes)?;

    Ok(u16::from_be_bytes(bytes))
}

fn extract32<const N: usize>(buf: &mut Reader<N>) -> Result<u32, BubbleError> {
    let mut bytes: [u8; 4] = [0u8; 4];

    buf.read_exact(&mut bytes)?;

    Ok(u32::from_be_bytes(bytes))
}

fn extract_len<const N: usize>(buf: &mut Reader<N>, len: usize) -> Result<Box<[u8]>, BubbleError> {
    let mut vec: Vec<u8> = Vec::with_capacity(len);

    for _ in 0..len {
        let mut byte: [u8; 1] = [0u8; 1];
        buf.read(&mut byte)?;

        let [item] = byte;
        vec.push(item);
    }

    Ok(vec.into_boxed_slice())
}

#[test]
fn test_decode_records() {
    let mock_url: [u8; 17] = [
        0x03, 0x77, 0x77, 0x77, 0x07, 0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65, 0x03, 0x63, 0x6f,
        0x6d, 0x00,
    ];

    let type_: u16 = TYPE_A;
    let class: u16 = 0;
    let ttl: u32 = 2345;
    let data: [u8; 4] = [127, 0, 0, 1];
    let data_len: u16 = 4;

    let mock_record: [u8; 31] = [
        &mock_url as &[u8],
        &(type_.to_be_bytes()),
        &(class.to_be_bytes()),
        &(ttl.to_be_bytes()),
        &(data_len.to_be_bytes()),
        &data,
    ]
    .concat()
    .try_into()
    .unwrap();

    let mut buf = Reader::new(mock_record);

    let records: Vec<Record> = decode_records(&mut buf, 1).unwrap();

    assert_eq!(records.len(), 1);

    for r in records {
        assert_eq!(r.name, "www.example.com");
        assert_eq!(r.ip(), Some(Ipv4Addr::new(127, 0, 0, 1)));
        assert_eq!(r.type_, TYPE_A);
        assert_eq!(r.class, 0);
        assert_eq!(r.ttl, 2345);
    }
}
