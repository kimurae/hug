// DNS Header
//  15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
// |                      ID                       |
// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
// |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
// |                    QDCOUNT                    |
// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
// |                    ANCOUNT                    |
// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
// |                    NSCOUNT                    |
// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
// |                    ARCOUNT                    |
// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

use bitfield::bitfield;

bitfield! {
    pub struct Header([u16]);
    impl Debug;

    pub u16, id, set_id : 15, 0;

    u8, into Rcode, response_code, set_response_code : 19, 16;

    bool, recursion_available, set_recursion_available : 23;
    bool, recursion_desired,   set_recursion_desired   : 24;
    bool, is_truncated,        set_is_truncated        : 25;
    bool, is_authority,        set_is_authority        : 26;

    u8, into OpCode, opcode, set_opcode : 30, 27;

    u8, into Qr    , question_or_response, set_question_or_response : 31, 31;

    pub u16, question_count,    set_question_count    : 47, 32;
    pub u16, answer_count,      set_answer_count      : 63, 48;
    pub u16, authority_count,   set_authority_count   : 79, 64;
    pub u16, additionals_count, set_additionals_count : 95, 80;
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Qr {
    Question,
    Response,
}

impl From<u8> for Qr {
    fn from(v: u8) -> Self {
        if v == 0 {
            Qr::Question
        } else {
            Qr::Response
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum OpCode {
    StandardQuery,
    NotUsed,
}

impl From<u8> for OpCode {
    fn from(v: u8) -> Self {
        if v == 0 {
            OpCode::StandardQuery
        } else {
            OpCode::NotUsed
        }
    }
}

impl Into<u16> for OpCode {
    fn into(self) -> u16 {
        match self {
            OpCode::StandardQuery => 0,
            _ => 1,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum Rcode {
    Ok,
    FormatError,
    ServerFailure,
    NameError,
    NotImplemented,
    Refused,
    NotUsed,
}

impl From<u8> for Rcode {
    fn from(v: u8) -> Self {
        match v {
            0b0000_0000 => Rcode::Ok,
            0b0000_0001 => Rcode::FormatError,
            0b0000_0010 => Rcode::ServerFailure,
            0b0000_0011 => Rcode::NameError,
            0b0000_0100 => Rcode::NotImplemented,
            0b0000_0101 => Rcode::Refused,
            _ => Rcode::NotUsed,
        }
    }
}

impl From<&[u8; 12]> for Header<[u16; 6]> {
    // Takes a byte stream of a specific format and
    // converts it to a Header.
    fn from(item: &[u8; 12]) -> Self {
        let mut chunks: [u16; 6] = [0u16; 6];

        for (idx, chunk) in item.chunks_exact(2).enumerate() {
            if idx > chunks.len() {
                break;
            }

            chunks[idx] = from8to16(Some(chunk));
        }

        Self(chunks)
    }
}

impl Default for Header<[u16; 6]> {
    fn default() -> Self {
        Header([0u16; 6])
    }
}

impl Header<[u16; 6]> {
    pub fn has_errors(&self) -> bool {
        match self.response_code() {
            Rcode::Ok => false,
            _ => true,
        }
    }
    pub fn is_question(&self) -> bool {
        self.question_or_response() == Qr::Question
    }

    pub fn serialize(&self) -> Vec<u8> {
        let bytes: [u16; 6] = self.0;

        bytes.into_iter().flat_map(|x| x.to_be_bytes()).collect()
    }
}

pub fn for_query(id: u16) -> Header<[u16; 6]> {
    let mut header: Header<[u16; 6]> = Default::default();

    header.set_id(id);
    header.set_question_count(1);

    return header;
}

fn from8to16(bytes: Option<&[u8]>) -> u16 {
    let pair = bytes.unwrap_or(&[0, 0]).try_into().unwrap_or([0, 0]);
    return u16::from_be_bytes(pair);
}

#[test]
fn test_from_u8() {
    {
        // Since we receive data as a big endian byte steam, that is what we should test for.
        let bytes: [u8; 12] = [0u8, 0x12, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8];

        let header = Header::from(&bytes);

        assert_eq!(header.id(), 0x12);
        assert_eq!(header.question_or_response(), Qr::Question);
        assert_eq!(header.opcode(), OpCode::StandardQuery);
        assert_eq!(header.is_authority(), false);
        assert_eq!(header.is_truncated(), false);
        assert_eq!(header.recursion_desired(), false);
        assert_eq!(header.recursion_available(), false);
        assert_eq!(header.response_code(), Rcode::Ok);
        assert_eq!(header.question_count(), 0);
        assert_eq!(header.answer_count(), 0);
        assert_eq!(header.authority_count(), 0);
        assert_eq!(header.additionals_count(), 0);
    }

    {
        // Since we receive data as a big endian byte steam, that is what we should test for.
        let bytes: [u8; 12] = [
            0u8,
            0x12,
            0b1000_0000,
            0u8,
            0u8,
            4u8,
            0u8,
            1u8,
            0u8,
            2u8,
            0u8,
            3u8,
        ];

        let header = Header::from(&bytes);

        assert_eq!(header.id(), 0x12);
        assert_eq!(header.question_or_response(), Qr::Response);
        assert_eq!(header.opcode(), OpCode::StandardQuery);
        assert_eq!(header.is_authority(), false);
        assert_eq!(header.is_truncated(), false);
        assert_eq!(header.recursion_desired(), false);
        assert_eq!(header.recursion_available(), false);
        assert_eq!(header.response_code(), Rcode::Ok);
        assert_eq!(header.question_count(), 4);
        assert_eq!(header.answer_count(), 1);
        assert_eq!(header.authority_count(), 2);
        assert_eq!(header.additionals_count(), 3);
    }

    {
        // Since we receive data as a big endian byte steam, that is what we should test for.
        let bytes: [u8; 12] = [
            0u8,
            0x12,
            0b1000_0100,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
        ];

        let header = Header::from(&bytes);

        assert_eq!(header.id(), 0x12);
        assert_eq!(header.question_or_response(), Qr::Response);
        assert_eq!(header.opcode(), OpCode::StandardQuery);
        assert_eq!(header.is_authority(), true);
        assert_eq!(header.is_truncated(), false);
        assert_eq!(header.recursion_desired(), false);
        assert_eq!(header.recursion_available(), false);
        assert_eq!(header.response_code(), Rcode::Ok);
    }

    {
        // Since we receive data as a big endian byte steam, that is what we should test for.
        let bytes: [u8; 12] = [
            0u8,
            0x12,
            0b1000_0010,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
        ];

        let header = Header::from(&bytes);

        assert_eq!(header.id(), 0x12);
        assert_eq!(header.question_or_response(), Qr::Response);
        assert_eq!(header.opcode(), OpCode::StandardQuery);
        assert_eq!(header.is_authority(), false);
        assert_eq!(header.is_truncated(), true);
        assert_eq!(header.recursion_desired(), false);
        assert_eq!(header.recursion_available(), false);
        assert_eq!(header.response_code(), Rcode::Ok);
    }

    {
        // Since we receive data as a big endian byte steam, that is what we should test for.
        let bytes: [u8; 12] = [
            0u8,
            0x12,
            0b1000_0001,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
        ];

        let header = Header::from(&bytes);

        assert_eq!(header.id(), 0x12);
        assert_eq!(header.question_or_response(), Qr::Response);
        assert_eq!(header.opcode(), OpCode::StandardQuery);
        assert_eq!(header.is_authority(), false);
        assert_eq!(header.is_truncated(), false);
        assert_eq!(header.recursion_desired(), true);
        assert_eq!(header.recursion_available(), false);
        assert_eq!(header.response_code(), Rcode::Ok);
    }

    {
        // Since we receive data as a big endian byte steam, that is what we should test for.
        let bytes: [u8; 12] = [
            0u8,
            0x12,
            0b1000_0000,
            0b1000_0000,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
        ];

        let header = Header::from(&bytes);

        assert_eq!(header.id(), 0x12);
        assert_eq!(header.question_or_response(), Qr::Response);
        assert_eq!(header.opcode(), OpCode::StandardQuery);
        assert_eq!(header.is_authority(), false);
        assert_eq!(header.is_truncated(), false);
        assert_eq!(header.recursion_desired(), false);
        assert_eq!(header.recursion_available(), true);
        assert_eq!(header.response_code(), Rcode::Ok);
    }

    {
        // Since we receive data as a big endian byte steam, that is what we should test for.
        let bytes: [u8; 12] = [
            0u8,
            0x12,
            0b1000_0000,
            0b0000_0010,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
            0u8,
        ];

        let header = Header::from(&bytes);

        assert_eq!(header.id(), 0x12);
        assert_eq!(header.question_or_response(), Qr::Response);
        assert_eq!(header.opcode(), OpCode::StandardQuery);
        assert_eq!(header.is_authority(), false);
        assert_eq!(header.is_truncated(), false);
        assert_eq!(header.recursion_desired(), false);
        assert_eq!(header.recursion_available(), false);
        assert_eq!(header.response_code(), Rcode::ServerFailure);
    }
}

#[test]
fn foo() {
    let id = 14u16;
    let bytes = u16::from_be_bytes([0b1000_0000, 0b0000_0010]);

    let flags = Header([id, bytes, 1u16, 0u16, 0u16, 0u16]);

    println!("{:?}", flags);
    println!("{:?}", flags.question_or_response());
}
