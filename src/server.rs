//! an abstraction for listening and polling for events
//! * When a packet is received over UDP: It dispatches.
//! * When a terminating signal is caught: It exits.

use std::net::UdpSocket;

use libc::{c_int, SIGQUIT, SIGSTOP, SIGTERM};
use log::error;
use polling::{Event, Poller};
use signal_hook::consts::TERM_SIGNALS;

use crate::{signals::Signals, BubbleError};

const UDP_REQUEST: usize = 0;
const TERMINATION_REQUEST: usize = 1;

/// Server event listener/poller.
///
/// Example
/// ```
/// use std::net::{Ipv4Addr, SocketAddrV4, UdpSocket},
/// // Binds to any ip available.
/// let bind = Ipv4Addr::new(0,0,0,0);
/// // DNS typically runs off port 53.
/// let port = 53u16;
/// let addr = SocketAddrV4::new(bind, port);
/// let socket = UdpSocket::bind(addr).expect("Unable to bind to 0.0.0.0:53");
///
/// let mut server = Server::new(socket.try_clone().expect("Unable to clone a socket."));
///
/// // In this closure you would put your dispatch call that responds to receiving data from
/// // UDP on Port 53.
/// server.on_request(|| println!("I haz request!"));
///
/// // This is where you place your worker run logic, it runs on every event loop.
/// server.run(|| -> Result<(), Infallible> {
///     println!("Running.");
///     Ok(())
/// }).unwrap();
///
/// drop(socket);
/// ```
pub struct Server<F: FnMut()> {
    request_handler: Option<F>,
    socket: UdpSocket,
}

impl<F: FnMut()> Server<F> {
    /// creates a new instance.
    pub fn new(socket: UdpSocket) -> Self {
        Self {
            socket,
            request_handler: None,
        }
    }

    /// Sets the callback that runs whenever a udp packet is received.
    pub fn on_request(&mut self, handler: F) {
        self.request_handler = Some(handler);
    }

    /// Runs the event loop, this will loop infinately until a termination
    /// signal or a fatal error is received.
    pub fn run<W>(&mut self, mut work: W) -> Result<(), BubbleError>
    where
        W: FnMut() -> Result<(), BubbleError>,
    {
        let mut events: Vec<Event> = Vec::new();

        let poller = Poller::new()?;

        let mut term_trap = Signals::new(TERM_SIGNALS)?;

        poller.add(term_trap.pipe(), Self::termination_event())?;

        poller.add(&self.socket, Self::udp_event())?;

        loop {
            events.clear();

            // In terms of a worker error, we want things to
            // stop normally.
            if let Err(e) = work() {
                error!("Fatal error: {}", e);
                return Ok(());
            };

            poller.wait(&mut events, None)?;

            for ev in &events {
                match ev.key {
                    UDP_REQUEST => {
                        if let Some(f) = &mut self.request_handler {
                            f();
                        };

                        poller.modify(&self.socket, Self::udp_event())?;
                    }
                    TERMINATION_REQUEST => {
                        if term_trap
                            .pending()
                            .any(|x| matches!(x as c_int, SIGTERM | SIGQUIT | SIGSTOP))
                        {
                            return Ok(());
                        }

                        poller.modify(term_trap.pipe(), Self::termination_event())?;
                    }
                    _ => {}
                }
            }
        }
    }

    fn termination_event() -> Event {
        Event::writable(TERMINATION_REQUEST)
    }

    fn udp_event() -> Event {
        Event::readable(UDP_REQUEST)
    }
}
