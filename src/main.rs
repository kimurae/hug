#![warn(missing_docs)]
//! A DNS Resolver (like bind) written in Rust.
//! ## Why?
//! To learn Rust and the DNS Spec :)
//! ## Current Status
//! - It can run, but has not been tested as a daemon (yet),
//! - It handles basic TYPE_A requests including recursion.
//! - However, it mostly ignores the header fields and question type, etc of the incomming query.
//! ## DNS Spec
//! TBD :)
use std::{
    net::{Ipv4Addr, SocketAddrV4, UdpSocket},
    sync::{
        atomic::AtomicBool,
        mpsc::{channel, sync_channel, Receiver, Sender, SyncSender},
        Arc, Mutex,
    },
};

use clap::Parser;
use log::{error, info, warn, LevelFilter};

use server::Server;
use signal_hook::consts::TERM_SIGNALS;
use signal_hook::flag;

use simple_logger::SimpleLogger;

#[cfg(target_os = "linux")]
use systemd_journal_logger::{connected_to_journal, JournalLog};

mod dispatch;
mod dns;
mod request;
mod server;
mod signals;

use dispatch::{Dispatcher, Task, TaskListener, UdpListener, Worker};
use request::{MessageReceiver, MessageSender, RequestFuture, RequestMap};

const MAX_QUEUED_TASKS: usize = 10_000;

type BubbleError = Box<dyn std::error::Error>;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[arg(default_value_t = Ipv4Addr::new(0,0,0,0), short, long)]
    bind: Ipv4Addr,
    #[arg(default_value_t = 53, short, long)]
    port: u16,
    #[arg(default_value_t = LevelFilter::Info, short, long)]
    verbosity: LevelFilter,
}

#[cfg(target_os = "linux")]
fn setup_log(verbosity: &LevelFilter) -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    if connected_to_journal() {
        JournalLog::default().install()?;
    } else {
        SimpleLogger::new().init()?;
    }

    log::set_max_level(*verbosity);

    Ok(())
}

#[cfg(not(target_os = "linux"))]
fn setup_log(verbosity: &LevelFilter) -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    SimpleLogger::new().with_colors(true).init()?;
    log::set_max_level(*verbosity);
    Ok(())
}

fn main() -> Result<(), BubbleError> {
    let args = Cli::parse();

    setup_log(&args.verbosity)?;

    let term_now = Arc::new(AtomicBool::new(false));

    // Kill Switch Engage!
    for sig in TERM_SIGNALS {
        flag::register_conditional_shutdown(*sig, 1, Arc::clone(&term_now))?;
        flag::register(*sig, Arc::clone(&term_now))?;
    }

    let addr = SocketAddrV4::new(args.bind, args.port);
    let requests = Arc::new(Mutex::new(RequestMap::new()));
    let socket = UdpSocket::bind(addr)?;

    let mut server = Server::new(socket.try_clone()?);

    let (message_producer, message_consumer): (Sender<_>, Receiver<_>) = channel();

    let (task_producer, task_consumer): (SyncSender<_>, Receiver<_>) =
        sync_channel(MAX_QUEUED_TASKS);

    socket.set_nonblocking(true)?;

    info!("Listening on {:?}", socket.local_addr()?);

    let request_future = RequestFuture::new(
        MessageReceiver(message_consumer),
        requests,
        MessageSender(socket.try_clone()?),
    );

    let task = Task {
        perform: Mutex::new(Some(request_future.perform())),
        producer: task_producer,
    };

    let worker: Worker<TaskListener> = dispatch::spawn(task, TaskListener(task_consumer))?;

    server.on_request(|| {
        if let Err(e) = dispatch::handle_socket(Dispatcher(&message_producer), UdpListener(&socket))
        {
            error!("[Main] {:?}", e);
        }
    });

    // This is temporary, until I determine how we will add multiple workers with threading.
    // Ideally I have a worker error event where the thread is cleaned up, and
    // based on the error, a new worker is spawned.
    // I can call it a supervisor, hahahaha!
    server.run(|| -> Result<(), BubbleError> { worker.run() })?;

    info!("[Dispatch] Received Terminate, Cleaning up.");

    message_producer.send(dispatch::Message::Stop)?;

    drop(socket);

    Ok(())
}
