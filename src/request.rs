use std::{
    io,
    net::{ToSocketAddrs, UdpSocket},
    sync::mpsc::{Receiver, TryRecvError},
};


use crate::dispatch::Message;

mod handler;
mod request_future;
mod request_map;

use handler::{ReceivesMessages, UdpResponder};

pub(crate) use request_future::RequestFuture;
pub(crate) use request_map::RequestMap;

pub(crate) struct MessageReceiver(pub(crate) Receiver<Message>);

impl ReceivesMessages for MessageReceiver {
    fn try_recv(&self) -> Result<Message, TryRecvError> {
        self.0.try_recv()
    }
}

pub(crate) struct MessageSender(pub(crate) UdpSocket);

impl UdpResponder for MessageSender {
    fn send_to<A: ToSocketAddrs>(&self, buf: &[u8], addr: A) -> io::Result<usize> {
        self.0.send_to(buf, addr)
    }
}
