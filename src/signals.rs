// heavily based off the Mio adapter https://github.com/vorner/signal-hook/blob/dd6ccc7aad1cd22687093e9e16a49675982e7ee8/signal-hook-mio/src/lib.rs
//
use libc::c_int;

use std::borrow::Borrow;
use std::io::Error;

// Might TRY and implement a windows version later...
use std::os::unix::{
    io::{AsRawFd, RawFd},
    net::UnixStream as Pipe,
};

use polling::Source;
use signal_hook::iterator::backend::{self, SignalDelivery};
use signal_hook::iterator::exfiltrator::{Exfiltrator, SignalOnly};

/// A struct which mimics [`signal_hook::iterator::SignalsInfo`]
/// but also allows usage together with the polling library.
pub struct SignalsInfo<E: Exfiltrator = SignalOnly>(SignalDelivery<Pipe, E>);

pub use backend::Pending;

impl<E: Exfiltrator> SignalsInfo<E> {
    /// Create a `Signals` instance.
    ///
    /// This registers all the signals listed. The same restrictions (panics, errors) apply
    /// as with [`Handle::add_signal`][backend::Handle::add_signal].
    pub fn new<I, S>(signals: I) -> Result<Self, Error>
    where
        I: IntoIterator<Item = S>,
        S: Borrow<c_int>,
        E: Default,
    {
        Self::with_exfiltrator(signals, E::default())
    }

    /// A constructor with specifying an exfiltrator to pass information out of the signal
    /// handlers.
    pub fn with_exfiltrator<I, S>(signals: I, exfiltrator: E) -> Result<Self, Error>
    where
        I: IntoIterator<Item = S>,
        S: Borrow<c_int>,
    {
        let (read, write) = Pipe::pair()?;

        //read.set_nonblocking(true)?;
        //write.set_nonblocking(true)?;

        let delivery = SignalDelivery::with_pipe(read, write, exfiltrator, signals)?;
        Ok(Self(delivery))
    }

    /// Returns an iterator of already received signals.
    ///
    /// This returns an iterator over all the signal numbers of the signals received since last
    /// time they were read (out of the set registered by this `Signals` instance). Note that they
    /// are returned in arbitrary order and a signal number is returned only once even if it was
    /// received multiple times.
    ///
    /// This method returns immediately (does not block) and may produce an empty iterator if there
    /// are no signals ready. So you should register an instance of this struct at an instance of
    /// [`mio::Poll`] to query for readability of the underlying self pipe.
    pub fn pending(&mut self) -> Pending<E> {
        self.0.pending()
    }
    pub fn pipe(&self) -> &Pipe {
        self.0.get_read()
    }
}

/// A simplified signal iterator.
///
/// This is the [`SignalsInfo`], but returning only the signal numbers. This is likely the
/// one you want to use.
pub type Signals = SignalsInfo<SignalOnly>;

impl AsRawFd for Signals {
    fn as_raw_fd(&self) -> RawFd {
        self.0.get_read().as_raw_fd()
    }
}

impl Source for Signals {
    fn raw(&self) -> RawFd {
        self.0.get_read().as_raw_fd()
    }
}
