#!/bin/bash

set mode +x

RUSTFLAGS="-C instrument-coverage" cargo test --tests

xcrun llvm-profdata merge -sparse default_*.profraw -o hug.profdata

xcrun llvm-cov show $(\
  for file in $(\
    RUSTFLAGS="-C instrument-coverage" \
      cargo test --tests --no-run --message-format=json \
        | jq -r "select(.profile.test == true) | .filenames[]" \
        | grep -v dSYM - \
  ); \
  do \
    printf "%s %s " -object $file;\
  done \
  ) \
  --instr-profile=hug.profdata \
  --Xdemangler=rustfilt \
  --show-instantiations \
  --show-line-counts-or-regions \
  --ignore-filename-regex='/.cargo/registry' \
  --use-color
