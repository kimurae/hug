
systemd = /etc/systemd/system
systemd_target = $(systemd)-$(wildcard $(systemd))
systemd_present = $(systemd)-$(systemd)
systemd_absent = $(systemd)-

launchd = /Library/LaunchDaemons
launchd_target = $(launchd)-$(wildcard $(launchd))
launchd_present = $(launchd)-$(launchd)
launchd_absent = $(launchd)-


.PHONY: all clean coverage default $(systemd_target) $(launchd_target)

all: debug target/release/hugd

clean:
	@cargo clean

coverage:
	@bash +x scripts/coverage.sh

default: debug

debug:
	@cargo build

install: target/release/hugd | $(systemd_target) $(launchd_target)

$(systemd_absent): ;

$(launchd_absent): ;

$(systemd_present): target/release/hugd /etc/systemd/system/hugd.service
	@mkdir -p /usr/local/sbin
	@sudo systemctl stop hugd.service
	@cp $< /usr/local/sbin/hugd
	@cp ./deploy/man/hugd.8 /usr/local/share/man/man8/hug.8
	@sudo systemctl start hugd.service

$(launchd_present): target/release/Hugd.pkg

/etc/systemd/system/hugd.service: ./deploy/systemd/hugd.service
	@cp $< $@
	@sudo systemctl daemon-reload

target/release/hugd:
	@cargo build --release

target/release/Hugd.pkg: target/.destroot/usr/local/libexec/hugd target/.destroot/usr/local/share/man/man8/hugd.8 target/.destroot/Library/LaunchDaemons/com.kimurae.hugd.plist
	find target/.destroot -name .DS_Store -delete
	productbuild --root target/.destroot $@

target/.destroot/usr/local/libexec/hugd: target/release/hugd
	mkdir -p $(@D)
	cp $< $@

target/.destroot/usr/local/share/man/man8/hugd.8: ./deploy/man/hugd.8
	mkdir -p $(@D)
	cp $< $@

target/.destroot/Library/LaunchDaemons/com.kimurae.hugd.plist: ./deploy/launchd/com.kimurae.hugd.plist
	mkdir -p $(@D)
	cp $< $@
