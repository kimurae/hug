FROM rust:1

ENV RUSTFLAGS="-C target-feature=+crt-static"

WORKDIR /app

COPY ./Makefile ./Makefile
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
COPY ./src ./src

RUN make install

CMD ["/usr/local/sbin/hugd"]
